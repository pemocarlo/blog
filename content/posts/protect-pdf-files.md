+++
title = "Protect PDF files from copying"
date = 2019-04-18T14:05:00+02:00
draft = false
author = "Carlos Perez"
+++

I have finished the task of protecting a list of PDFs from copying, without
adding a password. It means that the user should be able to open and read the
PDF normally, but it won't work once the copy of the content is copied with
Ctrl-c. After looking for diferent options, the only way to do it reliably is,
as always, the command line. And the best program for the task is QPDF. This an
advanced tool that can convert one PDF file to another equivalent PDF file by
applying different transformations, such as linearization, encryption and
decryption. The manual should be available at the link
<http://qpdf.sourceforge.net/> but it is broken. Fortunately, by writing the
command

{{< highlight bash >}}
qpdf --help
{{< /highlight >}}

you will get a complete manual. If you are in a terminal, it might be wiser to
pipe it into a pager

{{< highlight bash >}}
qpdf --help | less
{{< /highlight >}}

Encryption here does not mean only to add a password to your file. Contretely,
the command line options for encypting allow to set user and owner credentials,
select a key length and a list of different options to precisely control what
the user can/cannot access

Useful links

[string - Extract filename and extension in Bash - Stack Overflow](https://stackoverflow.com/questions/965053/extract-filename-and-extension-in-bash?page=1&tab=votes#tab-top)
[Encrypt, decrypt and process your PDFs with QPDF](https://betanews.com/2014/06/12/encrypt-decrypt-and-process-your-pdfs-with-qpdf/)
