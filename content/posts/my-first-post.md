+++
title = "My first blog post"
date = 2018-12-02T20:53:00+01:00
draft = false
author = "Carlos Perez"
+++

This is the first blog post for my new Hugo Blog. I will use it to test
functionalities, therefore it will keep growing and improving.

\\[ \frac{\hbar^2}{2m}\nabla^2\Psi + V(\mathbf{r})\Psi = -i\hbar
\frac{\partial\Psi}{\partial t} \\]

{{< highlight python >}}
print("hello world")
print("hello world")
print("hello world")
print("hello world")
print("hello world")
print("hello world")
{{< /highlight >}}

```text
hello world
hello world
hello world
hello world
hello world
hello world
```
